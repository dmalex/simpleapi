const express = require("express")

const app = express()

const routes = require("./routes")
app.use("/", routes)

const port = 8080

app.listen(port, () => {
    console.log(`Simple server API started on port ${port}`)
})
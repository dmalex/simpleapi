const express = require("express")
const router = express.Router()

const controller = require("./controller")

router.get("/", controller.getInfo)
router.get("/cities", controller.getCities)

module.exports = router